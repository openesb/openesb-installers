/*
 */

package com.sun.esb.install.cluster;

import java.util.HashMap;
import java.util.Map;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import org.apache.tools.ant.Task;

/**
 *
 * @author ylee
 */
public class ClusterTask extends Task {

    private String host = "localhost";
    private String jmxport = "8686";
    private String user = "admin";
    private String password = "adminadmin";


    public void execute() {
        // enable clustering support
        enableClusteringSupport(user,password,host,jmxport);

    }


    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(String port) {
        this.jmxport = port;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public MBeanServerConnection getServerConnection(String userName, String password, String hostname, String jmxport) {

        String urlString = "service:jmx:rmi:///jndi/rmi://"+hostname+":"+jmxport+"/jmxrmi";
        MBeanServerConnection connection = null;
        try {
            JMXServiceURL url = new JMXServiceURL(urlString);

            String[] credentials = new String[]{userName, password};
            Map<String, String[]> environment = new HashMap<String, String[]>();
            environment.put("jmx.remote.credentials", credentials);
            final JMXConnector connector = JMXConnectorFactory.connect(url,
                    environment);
            connection =  connector.getMBeanServerConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return connection;
    }


    public void enableClusteringSupport(String username, String password, String hostname, String jmxport) {

        MBeanServerConnection serverConnection = getServerConnection(username,password,hostname,jmxport);
        if (serverConnection != null) {
            String[] params = {"cluster"};
            String[] signatures = {"java.lang.String"};

            try {
                serverConnection.invoke(
                        new ObjectName("com.sun.appserv:type=domain,category=config"),
                        "addClusteringSupportUsingProfile", params, signatures);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


}
