/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU General
 * Public License Version 2 only ("GPL") or the Common Development and Distribution
 * License("CDDL") (collectively, the "License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html or nbbuild/licenses/CDDL-GPL-2-CP. See the
 * License for the specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header Notice in
 * each file and include the License file at nbbuild/licenses/CDDL-GPL-2-CP.  Sun
 * designates this particular file as subject to the "Classpath" exception as
 * provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the License Header,
 * with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original Software
 * is Sun Microsystems, Inc. Portions Copyright 1997-2007 Sun Microsystems, Inc. All
 * Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL or only the
 * GPL Version 2, indicate your decision by adding "[Contributor] elects to include
 * this software in this distribution under the [CDDL or GPL Version 2] license." If
 * you do not indicate a single choice of license, a recipient has the option to
 * distribute your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above. However, if
 * you add GPL Version 2 code and therefore, elected the GPL Version 2 license, then
 * the option applies only if the new code is made subject to such option by the
 * copyright holder.
 */
package org.netbeans.installer.products.nb.base;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.io.FileWriter;
import java.io.BufferedWriter;
import org.netbeans.installer.product.Registry;
import org.netbeans.installer.product.components.ProductConfigurationLogic;
import org.netbeans.installer.product.components.Product;
import org.netbeans.installer.product.filters.OrFilter;
import org.netbeans.installer.product.filters.ProductFilter;
import org.netbeans.installer.utils.FileProxy;
import org.netbeans.installer.utils.FileUtils;
import org.netbeans.installer.utils.LogManager;
import org.netbeans.installer.utils.StringUtils;
import org.netbeans.installer.utils.SystemUtils;
import org.netbeans.installer.utils.system.UnixNativeUtils;
import org.netbeans.installer.utils.applications.JavaUtils;
import org.netbeans.installer.utils.applications.JavaUtils.JavaInfo;
import org.netbeans.installer.utils.applications.NetBeansUtils;
import org.netbeans.installer.utils.exceptions.InitializationException;
import org.netbeans.installer.utils.exceptions.InstallationException;
import org.netbeans.installer.utils.exceptions.NativeException;
import org.netbeans.installer.utils.exceptions.UninstallationException;
import org.netbeans.installer.utils.helper.FilesList;
import org.netbeans.installer.utils.helper.RemovalMode;
import org.netbeans.installer.utils.helper.UiMode;
import org.netbeans.installer.utils.helper.Status;
import org.netbeans.installer.utils.helper.Text;
import org.netbeans.installer.utils.progress.Progress;
import org.netbeans.installer.utils.system.shortcut.FileShortcut;
import org.netbeans.installer.utils.system.shortcut.LocationType;
import org.netbeans.installer.utils.system.shortcut.Shortcut;
import org.netbeans.installer.wizard.Wizard;
import org.netbeans.installer.wizard.components.WizardComponent;
import org.netbeans.installer.wizard.components.panels.JdkLocationPanel;

/**
 *
 * @author Kirill Sorokin
 */
public class ConfigurationLogic extends ProductConfigurationLogic {
    /////////////////////////////////////////////////////////////////////////////////
    // Instance
    private List<WizardComponent> wizardComponents;

    public ConfigurationLogic() throws InitializationException {
        wizardComponents = Wizard.loadWizardComponents(
                WIZARD_COMPONENTS_URI,
                getClass().getClassLoader());
    }

    public void install(final Progress progress) throws InstallationException {
        final Product product = getProduct();
        final File installLocation = product.getInstallationLocation();
        final FilesList filesList = product.getInstalledFiles();
        final File binSubdir = new File(installLocation, BIN_SUBDIR);

        /////////////////////////////////////////////////////////////////////////////
        final File jdkHome = new File(
                product.getProperty(JdkLocationPanel.JDK_LOCATION_PROPERTY));
        try {
            progress.setDetail(getString("CL.install.jdk.home")); // NOI18N

            // make sure netbeans.conf can be modified
            if (!SystemUtils.isWindows()) {
                SystemUtils.executeCommand(
                        "chmod",
                        "u+rwx",
                        binSubdir.getAbsolutePath() + "/openesb");
            }

            JavaInfo info = JavaUtils.getInfo(jdkHome);
            LogManager.log("Using the following JDK for NetBeans configuration : ");
            LogManager.log("... path    : "  + jdkHome);
            if (info != null) {
                LogManager.log("... version : "  + info.getVersion().toJdkStyle());
                LogManager.log("... vendor  : "  + info.getVendor());
                LogManager.log("... final   : "  + (!info.isNonFinal()));
            }

            NetBeansUtils.setJavaHome(installLocation, jdkHome);

        } catch (IOException e) {
            throw new InstallationException(
                    getString("CL.install.error.jdk.home"), // NOI18N
                    e);
        }

        /////////////////////////////////////////////////////////////////////////////

        // This seems to be quite irrelevant now...
        //try {
        //    progress.setDetail(getString("CL.install.netbeans.clusters")); // NOI18N
        //
        //    NetBeansUtils.addCluster(installLocation, PLATFORM_CLUSTER);
        //    NetBeansUtils.addCluster(installLocation, NB_CLUSTER);
        //    NetBeansUtils.addCluster(installLocation, IDE_CLUSTER);
        //    NetBeansUtils.addCluster(installLocation, XML_CLUSTER);
        //} catch (IOException e) {
        //    throw new InstallationException(
        //            getString("CL.install.error.netbeans.clusters"), // NOI18N
        //            e);
        //}

        /////////////////////////////////////////////////////////////////////////////
        try {
            progress.setDetail(getString("CL.install.product.id")); // NOI18N

            filesList.add(NetBeansUtils.createProductId(installLocation));
        } catch (IOException e) {
            throw new InstallationException(
                    getString("CL.install.error.product.id"), // NOI18N
                    e);
        }

        /////////////////////////////////////////////////////////////////////////////
        try {
            progress.setDetail(getString("CL.install.license.accepted")); // NOI18N

            filesList.add(
                    NetBeansUtils.createLicenseAcceptedMarker(installLocation));
        } catch (IOException e) {
            throw new InstallationException(
                    getString("CL.install.error.license.accepted"), // NOI18N
                    e);
        }

        /////////////////////////////////////////////////////////////////////////////
        //try {
        //    progress.setDetail(getString("CL.install.irrelevant.files")); // NOI18N
        //
        //    SystemUtils.removeIrrelevantFiles(binSubdir);
        //    SystemUtils.removeIrrelevantFiles(etcSubdir);
        //    SystemUtils.removeIrrelevantFiles(platformCluster);
        //    SystemUtils.removeIrrelevantFiles(nbCluster);
        //    SystemUtils.removeIrrelevantFiles(ideCluster);
        //    SystemUtils.removeIrrelevantFiles(xmlCluster);
        //} catch (IOException e) {
        //    throw new InstallationException(
        //            getString("CL.install.error.irrelevant.files"), // NOI18N
        //            e);
        //}

        /////////////////////////////////////////////////////////////////////////////
        //try {
        //    progress.setDetail(getString("CL.install.files.permissions")); // NOI18N
        //
        //    SystemUtils.correctFilesPermissions(binSubdir);
        //    SystemUtils.correctFilesPermissions(etcSubdir);
        //    SystemUtils.correctFilesPermissions(platformCluster);
        //    SystemUtils.correctFilesPermissions(nbCluster);
        //    SystemUtils.correctFilesPermissions(ideCluster);
        //    SystemUtils.correctFilesPermissions(xmlCluster);
        //} catch (IOException e) {
        //    throw new InstallationException(
        //            getString("CL.install.error.files.permissions"), // NOI18N
        //            e);
        //}

        /////////////////////////////////////////////////////////////////////////////
        String createDesktop = System.getProperty("OPENESB.CREATE.DESKTOP.ICON");
        if ((createDesktop != null && createDesktop.equalsIgnoreCase("true")) ||
             UiMode.getCurrentUiMode() == UiMode.SILENT){
                LogManager.logIndent(
                        "creating the desktop shortcut for NetBeans IDE"); // NOI18N
                if (!SystemUtils.isMacOS()) {
                    try {
                        progress.setDetail(getString("CL.install.desktop")); // NOI18N

                        if (SystemUtils.isCurrentUserAdmin()) {
                            LogManager.log(
                                    "... current user is an administrator " + // NOI18N
                                    "-- creating the shortcut for all users"); // NOI18N

                            SystemUtils.createShortcut(
                                    getDesktopShortcut(installLocation),
                                    LocationType.ALL_USERS_DESKTOP);

                            getProduct().setProperty(
                                    DESKTOP_SHORTCUT_LOCATION_PROPERTY,
                                    ALL_USERS_PROPERTY_VALUE);
                        } else {
                            LogManager.log(
                                    "... current user is an ordinary user " + // NOI18N
                                    "-- creating the shortcut for the current " + // NOI18N
                                    "user only"); // NOI18N

                            SystemUtils.createShortcut(
                                    getDesktopShortcut(installLocation),
                                    LocationType.CURRENT_USER_DESKTOP);

                            getProduct().setProperty(
                                    DESKTOP_SHORTCUT_LOCATION_PROPERTY,
                                    CURRENT_USER_PROPERTY_VALUE);
                        }
                    } catch (NativeException e) {
                        LogManager.unindent();

                LogManager.log(
                        getString("CL.install.error.desktop"), // NOI18N
                        e);
                    }
                } else {
                    LogManager.log(
                            "... skipping this step as we're on Mac OS"); // NOI18N
                }
                LogManager.logUnindent(
                        "... done"); // NOI18N

                /////////////////////////////////////////////////////////////////////////////
                LogManager.logIndent(
                        "creating the start menu shortcut for NetBeans IDE"); // NOI18N
                try {
                    progress.setDetail(getString("CL.install.start.menu")); // NOI18N

                    if (SystemUtils.isCurrentUserAdmin()) {
                        LogManager.log(
                                "... current user is an administrator " + // NOI18N
                                "-- creating the shortcut for all users"); // NOI18N

                        SystemUtils.createShortcut(
                                getStartMenuShortcut(installLocation),
                                LocationType.ALL_USERS_START_MENU);

                        getProduct().setProperty(
                                START_MENU_SHORTCUT_LOCATION_PROPERTY,
                                ALL_USERS_PROPERTY_VALUE);
                    } else {
                        LogManager.log(
                                "... current user is an ordinary user " + // NOI18N
                                "-- creating the shortcut for the current " + // NOI18N
                                "user only"); // NOI18N

                        SystemUtils.createShortcut(
                                getStartMenuShortcut(installLocation),
                                LocationType.CURRENT_USER_START_MENU);

                        getProduct().setProperty(
                                START_MENU_SHORTCUT_LOCATION_PROPERTY,
                                CURRENT_USER_PROPERTY_VALUE);
                    }
                    // This is to fix the start menu for solaris and linux.
                    if(SystemUtils.isLinux() ||SystemUtils.isSolaris()) {
                       ((UnixNativeUtils) SystemUtils.getNativeUtils()).updateApplicationsMenu();
                    }

                } catch (NativeException e) {
            LogManager.log(
                    getString("CL.install.error.start.menu"), // NOI18N
                    e);
                }
                LogManager.logUnindent(
                        "... done"); // NOI18N
        } // end of creating desktop icon

        /////////////////////////////////////////////////////////////////////////////
        try {
            progress.setDetail(getString("CL.install.netbeans.conf")); // NOI18N

            NetBeansUtils.updateNetBeansHome(installLocation);

            // final long xmx = NetBeansUtils.getJvmMemorySize(
            //         installLocation,
            //         NetBeansUtils.MEMORY_XMX);
            // if (xmx < REQUIRED_XMX_VALUE) {
                 NetBeansUtils.setJvmMemorySize(
                        installLocation,
                        NetBeansUtils.MEMORY_XMX,
                        REQUIRED_XMX_VALUE);

                 NetBeansUtils.setJvmMemorySize(
                        installLocation,
                        NetBeansUtils.MEMORY_XMS,
                        REQUIRED_XMS_VALUE);
            // }
        } catch (IOException e) {
            throw new InstallationException(
                    getString("CL.install.error.netbeans.conf"), // NOI18N
                    e);
        }

        //get bundled registry to perform further runtime integration
        //http://wiki.netbeans.org/NetBeansInstallerIDEAndRuntimesIntegration
        Registry bundledRegistry = new Registry();
        try {
            final String bundledRegistryUri = System.getProperty(
                    Registry.BUNDLED_PRODUCT_REGISTRY_URI_PROPERTY);

            bundledRegistry.loadProductRegistry(
                    (bundledRegistryUri != null) ? bundledRegistryUri : Registry.DEFAULT_BUNDLED_PRODUCT_REGISTRY_URI);
        } catch (InitializationException e) {
            LogManager.log("Cannot load bundled registry", e);
        }


        /////////////////////////////////////////////////////////////////////////////
        try {
            progress.setDetail(getString("CL.install.glassfish.integration")); // NOI18N

            final List<Product> glassfishes =
                    Registry.getInstance().getProducts("glassfish");
            for (Product glassfish: glassfishes) {
                if (glassfish.getStatus() == Status.INSTALLED) {
                    final File gfLocation = glassfish.getInstallationLocation();

                    if (gfLocation != null) {
                        NetBeansUtils.setJvmOption(
                                installLocation,
                                GLASSFISH_JVM_OPTION_NAME,
                                gfLocation.getAbsolutePath(),
                                true);
                        NetBeansUtils.setJvmOption(
                                installLocation,
                                DERBY_OPTION_NAME,
                                gfLocation.getAbsolutePath() + File.separator + "databases",
                                true);
                        break;
                    }
                }
            }

	    // Comment the ergonomics to disable the deactivation of the modules GFESBv22 needs (ie JavaEE, etc...)
            NetBeansUtils.commentNetbeansClusterEntry(installLocation,
                    "ergonomics1");

            // Disable NB registration remainder by -J-Dnb.registration.enabled=false
            NetBeansUtils.setJvmOption(
                                installLocation,
                                REGISTRATION_ENABLE,
                                "false",
                                true);

            String nbDir = installLocation.getAbsolutePath();
            int idx = nbDir.lastIndexOf(File.separator);
            String userDir = nbDir.substring(0, idx) + File.separator + ".netbeans" + File.separator + "openesb";
            NetBeansUtils.setUserDir(installLocation, new File(userDir));

            /*
            String derbyHome = nbDir.substring(0, idx) + File.separator + ".netbeans-derby";
            NetBeansUtils.setJvmOption(
                                installLocation,
                                DERBY_OPTION_NAME,
                                derbyHome,
                                true);
            */
        } catch (IOException e) {
            throw new InstallationException(
                    getString("CL.install.error.glassfish.integration"), // NOI18N
                    e);
        }

        // write a short cut file to start NB in APP folder
        String appDir = "";
        File appFile = null;
        if (SystemUtils.isMacOS()) {
            int idx = installLocation.getAbsolutePath().indexOf("/Contents/Resources/");
            appDir = installLocation.getAbsolutePath().substring(0, idx);
            appFile = new File(appDir).getParentFile();
        } else {
            appFile = installLocation.getParentFile();
        }
        final String nbExecName = (SystemUtils.isWindows() ? "openesb.exe" : "openesb");
        final String startNBFile = (SystemUtils.isWindows() ? "start_netbeans.bat" : "start_netbeans");
        final String startNBFilePath = appFile.getAbsolutePath() + File.separator + startNBFile;
        try {
            FileWriter fstream = new FileWriter(startNBFilePath);
            BufferedWriter out = new BufferedWriter(fstream);
            String content = (SystemUtils.isWindows() ? "@echo off\n" : "#!/bin/sh\n");
            content += "cd \"" + installLocation.getAbsolutePath() + File.separator + "bin" + "\"\n";
            content += (SystemUtils.isWindows() ? "start " : "sh ") + nbExecName + "\n";
            out.write(content);
            out.close();

            if (!SystemUtils.isWindows()) {
                SystemUtils.executeCommand(
                        "chmod",
                        "u+rwx",
                        startNBFilePath);
            }
        } catch (IOException e) {
            LogManager.log(e);
        }

        try {
            filesList.add(installLocation);
        } catch (IOException e) {
            LogManager.log(e);
        }
        /////////////////////////////////////////////////////////////////////////////
        progress.setPercentage(Progress.COMPLETE);
    }

    public void uninstall(final Progress progress) throws UninstallationException {
        final Product product = getProduct();
        final File installLocation = product.getInstallationLocation();

        NetBeansUtils.warnNetbeansRunning(installLocation);
        /////////////////////////////////////////////////////////////////////////////
        try {
            progress.setDetail(getString("CL.uninstall.start.menu")); // NOI18N

            final String shortcutLocation =
                    getProduct().getProperty(START_MENU_SHORTCUT_LOCATION_PROPERTY);

            if ((shortcutLocation == null) ||
                    shortcutLocation.equals(CURRENT_USER_PROPERTY_VALUE)) {
                SystemUtils.removeShortcut(
                        getStartMenuShortcut(installLocation),
                        LocationType.CURRENT_USER_START_MENU,
                        true);
            } else {
                SystemUtils.removeShortcut(
                        getStartMenuShortcut(installLocation),
                        LocationType.ALL_USERS_START_MENU,
                        true);
            }
        } catch (NativeException e) {
            LogManager.log(
                    getString("CL.uninstall.error.start.menu"), // NOI18N
                    e);
        }

        /////////////////////////////////////////////////////////////////////////////
        if (!SystemUtils.isMacOS()) {
            try {
                progress.setDetail(getString("CL.uninstall.desktop")); // NOI18N

                final String shortcutLocation = getProduct().getProperty(
                        DESKTOP_SHORTCUT_LOCATION_PROPERTY);

                if ((shortcutLocation == null) ||
                        shortcutLocation.equals(CURRENT_USER_PROPERTY_VALUE)) {
                    SystemUtils.removeShortcut(
                            getDesktopShortcut(installLocation),
                            LocationType.CURRENT_USER_DESKTOP,
                            false);
                } else {
                    SystemUtils.removeShortcut(
                            getDesktopShortcut(installLocation),
                            LocationType.ALL_USERS_DESKTOP,
                            false);
                }
            } catch (NativeException e) {
                LogManager.log(
                        getString("CL.uninstall.error.desktop"), // NOI18N
                        e);
            }
        }
        String appDir = "";
        File appFile = null;
        if (SystemUtils.isMacOS()) {
            int idx = installLocation.getAbsolutePath().indexOf("/Contents/Resources/");
            appDir = installLocation.getAbsolutePath().substring(0, idx);
            appFile = new File(appDir).getParentFile();
        } else {
            appFile = installLocation.getParentFile();
        }
        final String startNBFile = (SystemUtils.isWindows() ? "start_netbeans.bat" : "start_netbeans");
        File fileToDelete = new File(appFile, startNBFile);
        fileToDelete.delete();

        /////////////////////////////////////////////////////////////////////////////
        progress.setPercentage(Progress.COMPLETE);
    }

    public List<WizardComponent> getWizardComponents() {
        return wizardComponents;
    }

    @Override
    public String getSystemDisplayName() {
        return getString("CL.system.display.name");
    }

    @Override
    public boolean allowModifyMode() {
        return false;
    }

    @Override
    public boolean wrapForMacOs() {
        return true;
    }

    @Override
    public String getExecutable() {
        if (SystemUtils.isWindows()) {
            return EXECUTABLE_WINDOWS;
        } else {
            return EXECUTABLE_UNIX;
        }
    }

    @Override
    public String getIcon() {
        if (SystemUtils.isWindows()) {
            return ICON_WINDOWS;
        } else if (SystemUtils.isMacOS()) {
            return ICON_MACOSX;
        } else {
            return ICON_UNIX;
        }
    }

    @Override
    public Text getLicense() {
        return null;
    }
    // private //////////////////////////////////////////////////////////////////////
    private Shortcut getDesktopShortcut(final File directory) {
        return getShortcut(
                getString("CL.desktop.shortcut.name"), // NOI18N
                getString("CL.desktop.shortcut.description"), // NOI18N
                getString("CL.desktop.shortcut.path"), // NOI18N
                directory);
    }

    private Shortcut getStartMenuShortcut(final File directory) {
        if (SystemUtils.isMacOS()) {
            return getShortcut(
                    getString("CL.start.menu.shortcut.name.macosx"), // NOI18N
                    getString("CL.start.menu.shortcut.description"), // NOI18N
                    getString("CL.start.menu.shortcut.path"), // NOI18N
                    directory);
        } else {
            return getShortcut(
                    getString("CL.start.menu.shortcut.name"), // NOI18N
                    getString("CL.start.menu.shortcut.description"), // NOI18N
                    getString("CL.start.menu.shortcut.path"), // NOI18N
                    directory);
        }
    }

    private Shortcut getShortcut(
            final String name,
            final String description,
            final String relativePath,
            final File location) {
        final File icon;
        final File executable;

        if (SystemUtils.isWindows()) {
            icon = new File(location, ICON_WINDOWS);
        } else if (SystemUtils.isMacOS()) {
            icon = new File(location, ICON_MACOSX);
        } else {
            icon = new File(location, ICON_UNIX);
        }

        if (SystemUtils.isWindows()) {
            executable = new File(location, EXECUTABLE_WINDOWS);
        } else {
            executable = new File(location, EXECUTABLE_UNIX);
        }

        final FileShortcut shortcut = new FileShortcut(name, executable);

        shortcut.setDescription(description);
        shortcut.setCategories(SHORTCUT_CATEGORIES);
        shortcut.setFileName(SHORTCUT_FILENAME);
        shortcut.setIcon(icon);
        //shortcut.setRelativePath(relativePath);
        shortcut.setWorkingDirectory(location);
        shortcut.setModifyPath(true);

        return shortcut;
    }

    public RemovalMode getRemovalMode() {
        return RemovalMode.LIST;
    }

    @Override
    public Map<String, Object> getAdditionalSystemIntegrationInfo() {
        Map<String, Object> map = super.getAdditionalSystemIntegrationInfo();
        if (SystemUtils.isWindows()) {
            //TODO: get localized readme if it is available and matches current locale
            String readme = new File(getProduct().getInstallationLocation(), "readme.html").getAbsolutePath();
            map.put("DisplayVersion", getString("CL.system.display.version"));
            map.put("Publisher",      getString("CL.system.publisher"));
            map.put("URLInfoAbout",   getString("CL.system.url.about"));
            map.put("URLUpdateInfo",  getString("CL.system.url.update"));
            map.put("HelpLink",       getString("CL.system.url.support"));
            //map.put("Readme",         readme);
        }
        return map;
    }
    /////////////////////////////////////////////////////////////////////////////////
    // Constants
    public static final String WIZARD_COMPONENTS_URI =
            FileProxy.RESOURCE_SCHEME_PREFIX + // NOI18N
            "org/netbeans/installer/products/nb/base/wizard.xml"; // NOI18N

    public static final String BIN_SUBDIR =
            "bin"; // NOI18N
    public static final String ETC_SUBDIR =
            "etc"; // NOI18N

    public static final String NB_CLUSTER  =
            "{nb-cluster}"; // NOI18N

    public static final String EXECUTABLE_WINDOWS =
            BIN_SUBDIR + "/openesb.exe"; // NOI18N
    public static final String EXECUTABLE_UNIX =
            BIN_SUBDIR + "/openesb"; // NOI18N

    public static final String ICON_WINDOWS =
            EXECUTABLE_WINDOWS;
    public static final String ICON_UNIX =
            NB_CLUSTER + "/netbeans.png"; // NOI18N
    public static final String ICON_MACOSX =
            ETC_SUBDIR + "/openesb.icns"; // NOI18N

    public static final String SHORTCUT_FILENAME =
            "netbeans-{display-version}.desktop"; // NOI18N
    public static final String[] SHORTCUT_CATEGORIES = new String[] {
        "Development", // NOI18N
        "Java",// NOI18N
        "IDE"// NOI18N
    };

    public static final String REGISTRATION_ENABLE =
            "-Dnb.registration.enabled"; // NOI18N

    public static final String GLASSFISH_JVM_OPTION_NAME =
            "-Dcom.sun.aas.installRoot"; // NOI18N
    public static final String GLASSFISH_MOD_JVM_OPTION_NAME =
            "-Dorg.glassfish.v3.installRoot"; //NOI18N

    public static final String TOMCAT_JVM_OPTION_NAME_TOKEN =
            "-Dorg.netbeans.modules.tomcat.autoregister.token"; // NOI18N
    public static final String DERBY_OPTION_NAME =
            "-Dnetbeans.derby.system.home"; // NOI18N

    public static final long REQUIRED_XMX_VALUE =
            512 * NetBeansUtils.M;

    public static final long REQUIRED_XMS_VALUE =
            512 * NetBeansUtils.M;

    private static final String DESKTOP_SHORTCUT_LOCATION_PROPERTY =
            "desktop.shortcut.location"; // NOI18N

    private static final String START_MENU_SHORTCUT_LOCATION_PROPERTY =
            "start.menu.shortcut.location"; // NOI18N

    private static final String ALL_USERS_PROPERTY_VALUE =
            "all.users"; // NOI18N

    private static final String CURRENT_USER_PROPERTY_VALUE =
            "current.user"; // NOI18N

}
